const ClienteModel = require('./model')
const cliente = {};

const clientes = [
    {id:0 , nome:'Mateus'},
    {id:1 , nome:'João'},
]

cliente.list = function(req,res){
    ClienteModel.all(function(clientes){
        res.render( __dirname  + '/views/list' , { data:clientes });
    })
};

cliente.details = function(req, res){
    let id = req.params.id
    let cliente = clientes[id]
    if(!cliente){
        res.sendStatus(404)
    }else{
        res.render( __dirname  + '/views/details' , {data : cliente} )
    }
}

cliente.add = function(req,res){
    if(req.method == 'GET'){
        res.render( __dirname  + '/views/add' , {context:false})
    }else{
        let body = req.body
        let message;
        console.log(body)
        clientes.push(
            { id:clientes.length , nome:body.nome}
        )
        message = "Cliente cadastrado com sucesso.";
        res.render( __dirname  + '/views/add' , {context:message} )
    }
}

cliente.edit = function(req, res){
    console.log(req.body)
    let cliente = req.body
    clientes[cliente.id].nome = cliente.nome
    res.redirect( '/clientes' )
}

module.exports = cliente;