const express = require('express');
const router = express.Router();
const cliente = require('./index')

router.get('/' , cliente.list)

router.get('/details/:id' , cliente.details)
router.post('/details/:id' , cliente.edit)

router.get('/add' , cliente.add)
router.post('/add' , cliente.add)

module.exports = router;